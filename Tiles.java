enum Tile{
	BLANK,
	X,
	O
}
public class Tiles{
	String name;
	Tile tile;
	public Tiles(Tile tile){
		this.tile = tile;
		if(tile == Tile.values()[0]){
			this.name = "_";
		}if(tile == Tile.values()[1]){
			this.name = "x";
		}if(tile == Tile.values()[2]){
			this.name = "O";
		}
		
	}
	public String getName(){
		return this.name;
	}
	public String toString(){
		return this.name;
	}
	//public static void main(String[] args){
	//	Tiles tile1 = new Tiles(Tile.BLANK);
	//	System.out.print(tile1.getName());	
	//}
	
}