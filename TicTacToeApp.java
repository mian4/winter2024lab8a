import java.util.Scanner;

public class TicTacToeApp{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		System.out.println("Welcome to the Tic Tac Toe Game!");
		Board boardOne = new Board();
		boolean gameOver = false;
		int player = 1;
		Tile playerToken = Tile.X;
		
		while(gameOver == false){
			System.out.println(boardOne);
			System.out.println("Give me a row and a column:");
			int row = reader.nextInt();
			int col = reader.nextInt();
			if(player == 1){
				playerToken = Tile.X;
			}else{
				playerToken = Tile.O;
			}
			
			while(boardOne.placeToken(row, col, playerToken)==false){
				System.out.println("Reenter appropriate rows and columns");
				row = reader.nextInt();
				col = reader.nextInt();
				//System.out.println(boardOne);
			}
			//System.out.println(boardOne);

			if(boardOne.checkIfWinning(playerToken)){
				System.out.println("Player "+player+" is the Winner");
				gameOver = true;
			}else if(boardOne.checkIfFull()){
				System.out.println("Its a Tie!");
				gameOver = true;
			}else{
				player++;
				if(player > 2){
					player = 1;
				}
			}
		}
		
		
		
	}


}