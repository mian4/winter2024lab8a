public class Board{
	private Tiles[][] grid;
	public Board(){
		this.grid = new Tiles[3][3];//{{new Tiles(Tile.BLANK),Tile.BLANK,Tile.BLANK},{Tile.BLANK,Tiles.BLANK,Tile.BLANK},{Tile.BLANK,Tile.BLANK,Tile.BLANK}};
		for(int i = 0; i<grid.length; i++){
			for(int j = 0; j<grid[i].length; j++){
				grid[i][j] = new Tiles(Tile.BLANK);
				System.out.println("This the name "+grid[i][j].getName());

			}
		}
	}
	public String toString(){
		String board = "0 1 2 \n";
		for(Tiles[] s : grid){
			
			for(Tiles ti : s){
				board+=ti+" ";
			
			}
			board+="\n";
		}
		return board;
	}
	//public static void main(String[] args){
		//Board board1 = new Board();
		//System.out.println(board1);
		//board1.placeToken(1,2,Tile.x);
		//System.out.println(board1);

	//}
	public boolean placeToken(int row, int col, Tile playerToken){
		if(row >= 3 || col>=3 || row<0 || col<0){
			//System.out.println("1st return");
			return false;
		}
		if(grid[row][col].name.equals("_")){
			grid[row][col] = new Tiles(playerToken);
			//System.out.println("2st return");
			return true;
			}else{
			//System.out.println("3st return");
			return false;
			}
		
		/*	 if (row < 0 || row >= 3 || col < 0 || col >= 3) {
            return false;
        }
        if (grid[row-1][col-1] == new Tiles(Tile.BLANK) {
            grid[row-1][col-1] = new Tiles(playerToken);
            return true;
        }
        return false;*/
	}
	public boolean checkIfFull(){
		boolean check = true;
		for(int row = 0; row<grid.length; row++){
			for(int col = 0; col<grid.length; col++){
				if(grid[row][col].name.equals("_")){
					check = false;
				}
			}
		}
		return check;
			
	}
	private boolean checkIfWinningHorizontal(Tile playerToken){
		for(int row = 0; row<3; row++){
				//System.out.println("This thr row and token compare: "+grid[row][0].getName().equals(playerToken));
				

				if(grid[row][0].tile==playerToken && grid[row][1].tile==playerToken&& grid[row][2].tile==playerToken){
					return true;
				}
			}
			return false;
	}
	private boolean checkIfWinningVertical(Tile playerToken){
		for(int col = 0; col<3; col++){
			if(grid[0][col].tile==playerToken&&grid[1][col].tile==playerToken&&grid[2][col].tile==playerToken){
				return true;
			}
		}
		return false;
}
	public boolean checkIfWinning(Tile playerToken){
		if(checkIfWinningHorizontal(playerToken)||checkIfWinningVertical(playerToken)){
			return true;
		}
		return false;
	}
}